# 
# Create ssh and gpg keys for git
# 
#!/bin/bash
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa
gpg --gen-key
gpg --list-secret-keys --keyid-format LONG