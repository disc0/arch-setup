# 
# Arch Linux Desktop Setup by DiscoSkyline
# https://gitlab.com/disc0/arch-setup
# 
#!/bin/bash

username=$(whoami)

# Updates
sudo pacman -Syu --noconfirm

# Cinnamon Desktop
sudo pacman -S base-devel neovim man doas --noconfirm
sudo pacman -S xorg-server xorg-xinit lightdm-gtk-greeter --noconfirm
sudo pacman -S cinnamon cinnamon-control-center cinnamon-desktop cinnamon-menus cinnamon-screensaver cinnamon-session cinnamon-settings-daemon cinnamon-translations cjs muffin nemo gnome-terminal --noconfirm
sudo systemctl enable lightdm

# Let wheel group use doas
doas bash -c 'echo permit persist :wheel as root > /etc/doas.conf'

# AUR helper
cd ~
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# yay first run
yay --sudo doas --save
sudo pacman -R sudo
yay -Y --gendb
yay -Syu --devel
yay -Syu --devel --save

# Browser and ffmpeg compatibility
yay -S waterfox-g4-bin ffmpeg-compat-58 

# Download and Install Theme
doas pacman -S arc-gtk-theme papirus-icon-theme otf-ipafont noto-fonts-emoji  --noconfirm
yay -S lightdm-slick-greeter lightdm-settings vimix-cursors
gsettings set org.cinnamon.desktop.wm.preferences theme "Arc-Dark"
gsettings set org.cinnamon.desktop.interface gtk-theme "Arc-Dark"
gsettings set org.cinnamon.desktop.interface icon-theme "Papirus"
gsettings set org.cinnamon.desktop.interface cursor-theme "Vimix-cursors"

# Nemo extensions
doas pacman -S nemo-fileroller nemo-terminal nemo-preview nemo-image-converter --noconfirm
doas pacman -S bash-completion --noconfirm

# Virtualization
doas pacman -S virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat libguestfs --noconfirm
doas systemctl enable libvirtd --now
doas usermod -aG libvirt $username

# Misc
doas pacman -S qbittorrent seahorse gedit remmina
yay -S flameshot ttf-ms-fonts btop

# Config files
mkdir -p ~/.config
cp "disco-cfg.sh" ~/.config/
echo source ~/.config/disco-cfg.sh >> ~/.bashrc

# Not sure how to automate this yet
# At least it opens the file for now.
doas vim /etc/lightdm/lightdm.conf