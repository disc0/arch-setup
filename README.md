# Arch-Setup

Nice script to setup my cinnamon desktop, extensions, themes, and a better browser.

## Installation
Assuming you are using sudo.  
Just run setup.sh  

## Git key setup
Run git-setup.sh 

## Contributing
If I use it on my desktop, I'll add it. Open to suggestions.


## License
Apache License Version 2.0
